package com.chenye.netty.personalProtocol.client;

import com.chenye.netty.personalProtocol.FunDecoder;
import com.chenye.netty.personalProtocol.FunEncoder;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * @Auther:chenye
 * @Data:2018/8/16
 * @Description:
 */
public class FunClient {
    public static void main(String[] args){
        EventLoopGroup parent = new NioEventLoopGroup();
        try {
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(parent);
            bootstrap.channel(NioSocketChannel.class);

            bootstrap.handler(new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel ch) throws Exception {
                    ChannelPipeline channelPipeline = ch.pipeline();
                    channelPipeline.addLast(new FunEncoder());
                    channelPipeline.addLast(new FunDecoder());
                    channelPipeline.addLast(new ClientHandler());
                }
            });
            ChannelFuture future = bootstrap.connect("127.0.0.1",8080).sync();
            future.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            parent.shutdownGracefully();
        }
    }
}
