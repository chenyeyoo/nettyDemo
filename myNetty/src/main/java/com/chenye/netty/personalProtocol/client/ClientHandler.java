package com.chenye.netty.personalProtocol.client;

import com.chenye.netty.personalProtocol.FunEntity;
import com.chenye.netty.personalProtocol.FunHeader;
import io.netty.channel.ChannelHandlerAdapter;
import io.netty.channel.ChannelHandlerContext;

import java.util.UUID;

/**
 * @Auther:chenye
 * @Data:2018/8/16
 * @Description:
 */
public class ClientHandler extends ChannelHandlerAdapter {
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        System.err.println("接收到服务端返回消息："+msg);
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        String content = "测试";
        FunEntity funEntity = new FunEntity(content,new FunHeader(1,content.length(),UUID.randomUUID().toString()));
        System.err.println("发送消息:"+funEntity);
        ctx.writeAndFlush(funEntity);
    }
}
