package com.chenye.netty.personalProtocol;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * @Auther:chenye
 * @Data:2018/8/16
 * @Description:
 */
public class FunEncoder extends MessageToByteEncoder<FunEntity> {
    @Override
    protected void encode(ChannelHandlerContext ctx, FunEntity msg, ByteBuf out) throws Exception {
        //协议写入顺序
        FunHeader header = msg.getHeader();
        out.writeInt(header.getVersion());
        out.writeInt(header.getContentLength());
        out.writeBytes(header.getSessionId().getBytes());
        out.writeBytes(msg.getContent().getBytes());
    }
}
