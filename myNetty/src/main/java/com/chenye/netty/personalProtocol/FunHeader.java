package com.chenye.netty.personalProtocol;

/**
 * @Auther:chenye
 * @Data:2018/8/16
 * @Description:
 */
public class FunHeader {
    private int version;
    private int contentLength;
    private String sessionId;

    public FunHeader() {
    }

    public FunHeader(int version, int contentLength, String sessionId) {
        this.version = version;
        this.contentLength = contentLength;
        this.sessionId = sessionId;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getContentLength() {
        return contentLength;
    }

    public void setContentLength(int contentLength) {
        this.contentLength = contentLength;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    @Override
    public String toString() {
        return "FunHeader{" +
                "version=" + version +
                ", contentLength=" + contentLength +
                ", sessionId='" + sessionId + '\'' +
                '}';
    }
}
