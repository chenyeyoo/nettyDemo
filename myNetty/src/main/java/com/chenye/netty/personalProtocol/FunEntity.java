package com.chenye.netty.personalProtocol;

/**
 * @Auther:chenye
 * @Data:2018/8/16
 * @Description:
 */
public class FunEntity {
    private String content;
    private FunHeader header;

    public FunEntity() {
    }

    public FunEntity(String content, FunHeader header) {
        this.content = content;
        this.header = header;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public FunHeader getHeader() {
        return header;
    }

    public void setHeader(FunHeader header) {
        this.header = header;
    }

    @Override
    public String toString() {
        return "FunEntity{" +
                "content='" + content + '\'' +
                ", header=" + header +
                '}';
    }
}
