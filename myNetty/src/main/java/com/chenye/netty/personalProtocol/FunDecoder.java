package com.chenye.netty.personalProtocol;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/**
 * @Auther:chenye
 * @Data:2018/8/16
 * @Description:读取字节流里面的信息
 */
public class FunDecoder extends ByteToMessageDecoder {
    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        //读取协议传输的内容,顺序同FunEncoder
        int version = in.readInt();
        int length = in.readInt();
        String sessionId = new String(in.readBytes(36).array());
        String content = new String(in.readBytes(in.readableBytes()).array());
        out.add(new FunEntity(content,new FunHeader(version,length,sessionId)));
    }
}
